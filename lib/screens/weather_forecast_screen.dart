import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:forecast/models/weather_forecast_daily.dart';
import 'package:forecast/screens/city_screen.dart';
import 'package:forecast/widgets/city_view.dart';
import 'package:forecast/widgets/daily_forecast.dart';
import 'package:forecast/widgets/detail_view.dart';
import 'package:forecast/widgets/temp_view.dart';
import '../api/weather_api.dart';

class WeatherForecastScreen extends StatefulWidget {
  const WeatherForecastScreen({Key? key, this.locationWeather}) : super(key: key);
  final WeatherForecast? locationWeather;

  @override
  _WeatherForecastScreenState createState() => _WeatherForecastScreenState();
}

class _WeatherForecastScreenState extends State<WeatherForecastScreen> {

  late Future <WeatherForecast> forecastObject;
  late String _cityName;

  @override
  void initState() {
    super.initState();

    if (widget.locationWeather!=null) {
      forecastObject = Future.value(widget.locationWeather);
      //forecastObject = WeatherApi().fetchWeatherForecast(city: _cityName);
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text('Weather forecast'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.my_location_outlined),
          onPressed: (){
            setState((){
              forecastObject = WeatherApi().fetchWeatherForecast();
            });
          },color: Colors.white,
        ),

        actions: <Widget>[
          IconButton(icon: const Icon(Icons.search),
            onPressed: () async {
              var tappedName = await Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const CityScreen();
              }));
              if(tappedName!=null){
                  setState((){
                    _cityName = tappedName;
                    forecastObject = WeatherApi().fetchWeatherForecast(city: _cityName, isCity: true);
                  });
              }
            },color: Colors.white,
          ),
        ],
      ),
        backgroundColor:Colors.green[50],
        body: ListView(
          children: <Widget>[
            FutureBuilder<WeatherForecast>(
              future: forecastObject,
              builder: (context, snapshot){
                if(snapshot.hasData){
                  return Column(
                    children: <Widget>[
                      const SizedBox(height: 50.0,),
                      CityView(snapshot: snapshot),
                      const SizedBox(height: 50.0,),
                      TempView(snapshot: snapshot),
                      const SizedBox(height: 50.0,),
                      DetailView(snapshot: snapshot),
                      const SizedBox(height: 50.0,),
                      BottomListView(snapshot: snapshot),
                    ],
                  );
                } else {
                  return const Center(
                    child: SpinKitDoubleBounce(
                      color: Colors.blueAccent,
                      size: 50.0,
                    ),
                  );
                }
              },
            )
          ],
        ),
    );
  }
}
