import 'package:flutter/material.dart';

class CityScreen extends StatefulWidget {
  const CityScreen({Key? key}) : super(key: key);

  @override
  State<CityScreen> createState() => _CityScreenState();
}

class _CityScreenState extends State<CityScreen> {

  late String cityName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text('Location search'),
          centerTitle: true,
      ),

      body: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                cursorColor: Colors.blueGrey[800],
                style: const TextStyle(color: Colors.white),
                decoration: const InputDecoration(
                  hintText: 'Enter the name of the city',
                  hintStyle: TextStyle(color: Colors.white, fontSize: 20.0),
                  filled: true,
                  fillColor: Colors.cyan,
                  border: OutlineInputBorder(
                      borderRadius:
                      BorderRadius.all(Radius.circular(10.0),),
                      borderSide: BorderSide.none),
                  icon: Icon(
                    Icons.location_city,
                    color: Colors.teal,
                    size: 50.0,
                  ),
                ),
                onChanged: (value) {
                  cityName = value;
                },
              ),
            ),
            TextButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: const <Widget>[
                   Text(
                  'search',
                     style: TextStyle(fontSize: 25.0,color: Colors.teal),
                   ),],),
              onPressed: () {
                Navigator.pop(context, cityName);
              },
            ),
          ],
        ),
      ),
    );
  }
}