import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Util{

  static String getFormattedDate(DateTime dateTime){
    var now = DateTime.now();
    return (
      DateFormat('EEEE, MMMM d, y').format(now)
    );
  }

  static String getFormattedDateTime(DateTime dateTime){
    return (
        DateFormat('EEEE, MMMM d, y').format(dateTime)
    );
  }

  static getItem(IconData iconData, int value, String units) {
    return Column(
      children: <Widget> [
        Icon(iconData, color: Colors.black, size: 28.0,),
        const SizedBox(height: 10.0,),
        Text('$value', style: const TextStyle(fontSize: 20.0, color: Colors.black),),
        const SizedBox(height:  10.0),
        Text(units, style: const TextStyle(fontSize: 15.0, color: Colors.black),),
      ],
    );
  }
}