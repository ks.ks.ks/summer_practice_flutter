import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:forecast/utilities/forecast_util.dart';
import '../models/weather_forecast_daily.dart';

class DetailView extends StatelessWidget {
  const DetailView({Key? key, required this.snapshot}) : super(key: key);
  final AsyncSnapshot <WeatherForecast> snapshot;

  @override
  Widget build(BuildContext context) {

    var pressure = (snapshot.data?.list?[0].pressure)!*0.750062;
    var humidity = snapshot.data?.list?[0].humidity;
    var wind = snapshot.data?.list?[0].speed;
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget> [
        Util.getItem(FontAwesomeIcons.thermometer, pressure.round(),'mm Hg'),
        Util.getItem(FontAwesomeIcons.cloudRain, humidity!, '%'),
        Util.getItem(FontAwesomeIcons.wind, wind!.toInt(), 'km/H')
      ],
    );
  }
}
