import 'package:flutter/material.dart';
import 'package:forecast/utilities/forecast_util.dart';

Widget forecastCard(AsyncSnapshot snapshot, int index){

  var forecastList = snapshot.data?.list;
  var dayOfWeek = '';
  DateTime date = DateTime.fromMillisecondsSinceEpoch(forecastList?[index].dt*1000);
  var fullDate = Util.getFormattedDateTime(date);
  dayOfWeek = fullDate.split(',')[0];

  var tempMax = forecastList[index].temp.day.toStringAsFixed(0);
  var tempMin = forecastList[index].temp.night.toStringAsFixed(0);
  var icon = forecastList[index].getIconUrl();

  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget> [
      Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            dayOfWeek.toUpperCase(),
            style: const TextStyle(fontSize: 25.0,color: Colors.white),
          ),
        ),
      ),
       Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'day: $tempMax °C'.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 25.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Image.network(icon, scale: 0.9),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'night: $tempMin °C'.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 25.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Image.network(icon, scale: 0.9),
                ],
              ),
            ],
          ),
        ],
      ),
    ],
  );
}

