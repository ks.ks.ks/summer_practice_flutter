import 'package:flutter/material.dart';
import 'package:forecast/models/weather_forecast_daily.dart';

class TempView extends StatelessWidget {
  const TempView({Key? key, required this.snapshot}) : super(key: key);
  final AsyncSnapshot <WeatherForecast> snapshot;

  @override
  Widget build(BuildContext context) {

    var icon = snapshot.data?.list?[0].getIconUrl();
    var temp = snapshot.data?.list?[0].temp.eve.toStringAsFixed(0);
    var description = snapshot.data?.list?[0].weather[0].description.toUpperCase();
    var feelsLike = snapshot.data?.list?[0].feelsLike.eve.toStringAsFixed(0);

    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.network(icon!, scale: 0.4,),
          const SizedBox(width: 20.0,),
          Column(
            children: <Widget>[
              Text('$temp °C', style: const TextStyle(fontSize: 54.0, color: Colors.black),
              ),
              Text('$description', style: const TextStyle(fontSize: 18.0, color: Colors.black),),
              const Padding(padding: EdgeInsets.all(8.0)),
              Text('FEELS LIKE $feelsLike °C', style: const TextStyle(fontSize: 18.0, color: Colors.black),),
            ],
          ),
        ],
      ),
    );
  }
}
