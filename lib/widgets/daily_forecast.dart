import 'package:flutter/material.dart';
import 'package:forecast/models/weather_forecast_daily.dart';
import 'package:forecast/widgets/forecast_card.dart';

class BottomListView extends StatelessWidget {
  const BottomListView({Key? key, required this.snapshot}) : super(key: key);
  final AsyncSnapshot <WeatherForecast> snapshot;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text('7-Day weather forecast'.toUpperCase(),style: const TextStyle(
            fontSize: 20.0,
            color: Colors.black,
            fontWeight: FontWeight.bold),
        ),
        Container(
          height:200.0,
          padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
          child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context,index) => Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(colors: [Colors.teal,Colors.lightGreenAccent])
                ),
                width: MediaQuery.of(context).size.width,//.width/1.1,
                height: MediaQuery.of(context).size.height,
                child: forecastCard(snapshot,index),
              ),
              separatorBuilder:(context,index) => const SizedBox(width: 8,),
              itemCount:snapshot.data!.list!.length,
          ),
        ),
      ],
    );
  }
}
